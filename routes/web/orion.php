<?php

use App\Http\Controllers\ApiController;

Route::get('companies/get-companies',[ApiController::class,'getCompaniesList']);
Route::get('companies/get-address',[ApiController::class,'getAddressList']);
Route::get('companies/change-status/{id}/{target}',[ApiController::class,'changeStatus']);