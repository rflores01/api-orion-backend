<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use DB;
use Illuminate\Http\Request;

class ApiController extends BaseController {

    public function getCompaniesList() 
    {
        try
        {
            $result = DB::table('companies')->where('status',1)->get();

            return response()->json([
                'msg' => 'done!',
                'action' => true,
                'data' => $result
            ]); 
        }
        catch(\Exception $e)
        {
            return response()->json([
                'msg' => $e->getMessage(),
                'action' => false,
                'data' => []
            ]);
        }
    }

    public function getAddressList(Request $request) {
        try
        {
            $result = DB::table('address')->where('customer',$request->customer)->get();

            return response()->json([
                'msg' => 'done!',
                'action' => true,
                'data' => $result
            ]);
        }   
        catch(\Exception $e)
        {
            return response()->json([
                'msg' => $e->getMessage(),
                'action' => false,
                'data' => []
            ]);
        }    
    }

    public function changeStatus($id,$target) {
        try 
        {
            DB::table('address')->where('id',$id)->update([
                'status' => $target
            ]);

            return response()->json([
                'msg' => 'done!',
                'action' => true,
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'msg' => $e->getMessage(),
                'action' => false,
            ]);
        }
    } 

}